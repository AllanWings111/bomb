import Personaje


class JonSnow(Personaje.Personaje):
    def __init__(self, mapa, controlador):
        super().__init__()
        self.controlador = controlador
        self.advertencia_al_jugador = print("JonSnow tiene una zona segura, el"
                                            "spawn, donde nunca podra morir")
        self.mapa = mapa  # Defino al mapa ya que están vinculados
        self.vidas = 3  # La cantidad de oportunidades que tiene el bomerman
        self.direccion = 0
        self.dictionary = {"d": [[self.casilla[0] + 1, self.casilla[1]],
                                 [self.vertices_2[0] + self.velocidad,
                                 self.vertices_2[1]], [self.vertices_1[0] +
                                 self.velocidad, self.vertices_1[1]]],
                           "d1": [[self.casilla[0] + 1, self.casilla[1] + 1],
                                  [self.vertices_4[0] + self.velocidad,
                                  self.vertices_4[1]], self.vertices_1],
                           "d2": [self.x += self.velocidad * (self.x <= 655)],
                           "d3": [self.x, self.posicion[1]],
                           "d4": [self.casilla[0] +
                                  self.nueva_posicion_posible_parte_superior
                                  [1], self.casilla[1]],
                           "a": [[self.casilla[0] - 1, self.casilla[1]],
                                 [self.vertices_1[0] - self.velocidad,
                                  self.vertices_1[1]],
                                 [self.vertices_1[0] - 5 - 5, self.vertices_1
                                 [1]]],
                           "a1": [[self.casilla[0] - 1, self.casilla[1] + 1],
                                  [self.vertices_3[0] - self.velocidad,
                                  self.vertices_3[1]], [self.vertices_3[0] - 5,
                                  self.vertices_3[1]]],
                           "a2": [self.x -= self.velocidad * (self.x >= 15)],
                           "a3": [self.x, self.posicion[1]],
                           "a4": [self.casilla[0] -
                                  self.nueva_posicion_posible_parte_superior
                                  [1] *
                                  (self.nueva_posicion_posible_parte_inferior
                                  [0] != 1) *
                                  (self.nueva_posicion_posible_parte_superior
                                  [0] != 1), self.casilla[1]],
                           "s": [[self.casilla[0], self.casilla[1] + 1],
                                 [self.vertices_3[0], self.vertices_3[1] +
                                  self.velocidad], [self.vertices_1[0],
                                 self.vertices_1[1] + 5]],
                           "s1": [[self.casilla[0] + 1, self.casilla[1] + 1],
                                  [self.vertices_4[0], self.vertices_4[1] +
                                   self.velocidad], self.vertices_1],
                           "s2": [self.y += self.velocidad *
                                  (self.y <= 655)],
                           "s3": [self.posicion[0], self.y],
                           "s4": [self.casilla[0], self.casilla[1] +
                                  self.nueva_posicion_posible_parte_superior
                                  [1]],
                           "w": [[self.casilla[0], self.casilla[1] - 1],
                                 [self.vertices_1[0], self.vertices_1[1] -
                                  self.velocidad], [self.vertices_1[0],
                                 self.vertices_1[1] - 5 - 5]],
                           "w1": [[self.casilla[0] + 1, self.casilla[1] - 1],
                                  [self.vertices_2[0], self.vertices_2[1] -
                                   self.velocidad], [self.vertices_1[0],
                                  self.vertices_1[1]]],
                           "w2": [self.y -= self.velocidad * (self.y >= 15) *
                                  (self.nueva_posicion_posible_parte_inferior
                                  [0] != 1) *
                                  (self.nueva_posicion_posible_parte_superior
                                  [0] != 1)],
                           "w3": [self.posicion[0], self.y],
                           "w4": [self.casilla[0], self.casilla[1] -
                                  self.nueva_posicion_posible_parte_superior
                                  [1] *
                                  (self.nueva_posicion_posible_parte_inferior
                                  [0] != 1) *
                                  (self.nueva_posicion_posible_parte_superior
                                  [0] != 1)],
                           }

    def mover_bm(self, direc):
        """Evalua si el bomberman se puede mover hacia donde le indicamos
        segun las coordenadas de sus vertices"""
        self.direccion = direc  # recibe del mapa hacia donde se va a mover
        self.nueva_posicion_posible_parte_superior =
        self.mapa.consultar_casilla_por_movimiento(self.dictionary
                                                   [self.direccion])
        self.nueva_posicion_posible_parte_inferior =
        self.mapa.consultar_casilla_por_movimiento(self.dictionary
                                                   [self.direccion + "1"])
        if self.nueva_posicion_posible_parte_superior[0] != 1
        and
        self.nueva_posicion_posible_parte_inferior[0] != 1:
            self.dictionary[self.direccion + "2"]
        self.posicion = self.dictionary[self.direccion + "3"]
        self.casilla = self.dictionary[self.direccion + "4"]
        self.redefinir_vertices()
        return self.posicion

    def respawnear(self):
        """Envia al bomberman a la casilla de spawn"""
        self.casilla = [0, 0]
        self.vidas -= 1
        self.posicion = [15, 15]
        self.x = 15
        self.y = 15
        print('Vidas: ', self.vidas)
        self.lista_de_acitvidades = [self.reiniciar_partida, self.pasar,
                                     self.pasar, self.pasar]
        self.lista_de_acitvidades[self.vidas]()

    def set_id_casilla(self):
        """Devuelve el id de la casilla en el"""
        """que está parado en modo de tuple"""
        return (self.casilla[0], self.casilla[1])

    def reiniciar_partida(self):
        """Cuando las hps sean igual a 0 se reinicia el nivel"""
        print('Perdistes, empezas todo de nuevo')
        self.controlador.resetear_nivel(0)

    def redefinir_vertices(self):
        """Redefine las vertices (vertices) en funcion de los movimentos"""
        print('id_casilla', self.casilla, 'pos', self.posicion)
        self.nueva_posicion_posible_parte_inferior = [0, 0]
        self.nueva_posicion_posible_parte_superior = [0, 0]
        self.vertices_1 = self.posicion
        self.vertices_2 = [self.posicion[0] + self.medidas, self.posicion[1]]
        self.vertices_3 = [self.posicion[0], self.posicion[1] + self.medidas]
        self.vertices_4 = [self.posicion[0] + self.medidas, self.posicion[1] +
                           self.medidas]

    def pasar(self):
        pass
