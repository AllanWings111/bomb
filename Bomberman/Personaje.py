
class Personaje:
    def __init__(self):
        self.medidas = 65 # Medidas del Personaje, no hace falta declarar más por que es un cuadrado
        # Defino las posciones de todas las vertices del cuadrado que encapsula al personaje, que se usaran para consultar el estado del objeto
        self.nueva_posicion_posible_parte_inferior = [0,0]
        self.nueva_posicion_posible_parte_superior = [0,0]
        self.velocidad = 5 # Cuantos píxeles aumenta la x y la y del bm al moverse
        self.posicion = [15,15] # Posiciòn del objeto
        self.casilla = [0,0] # La casilla en donde esta el vertices  superior izquierdo
        self.x = 10 
        self.y = 10
        # Defino las posciones de todas las vertices del cuadrado que encapsula al bm, que se usaran para consultar el estado del objeto
        self.vertices_1 = self.posicion
        self.vertices_2 = [self.posicion[0] + self.medidas, self.posicion[1]]
        self.vertices_3 = [self.posicion[0], self.posicion[1] + self.medidas]
        self.vertices_4 = [self.posicion[0] + self.medidas, self.posicion[1] + self.medidas]


    def get_id_casilla(self):
        """Se devuelve el id casilla del objeto"""
        return self.casilla

    def get_posicion(self):
        """Se devuelve la posicion del objeto"""
        return self.posicion